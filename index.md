## Color

Colorは,色と,色に応じて変化するコンテンツのサンプルです。

### 色
- A\~R: 有彩色, S: 無彩色, T: 全色
- HSL表示において,HとSはそれぞれ,極座標におけるθとrの関係である。A\~Sの色は,Sが原点,A\~Rが中心から等しい距離だけ離れ,20°ずつずれた位置にあると考えられる。それを花の図で表現している。

### 色の対応
LightとDarkは背景色の値で表示している。

| カラー |        Vivid       |        Light       |        Dark        |
|:-----:|-------------------:|-------------------:|-------------------:|
|   A   |   `hsl(0,80%,50%)` |   `hsl(0,80%,95%)` |   `hsl(0,80%,15%)` |
|   B   |  `hsl(20,80%,50%)` |  `hsl(20,80%,95%)` |  `hsl(20,80%,15%)` |
|   C   |  `hsl(40,80%,50%)` |  `hsl(40,80%,95%)` |  `hsl(40,80%,15%)` |
|   D   |  `hsl(60,80%,50%)` |  `hsl(60,80%,95%)` |  `hsl(60,80%,15%)` |
|   E   |  `hsl(80,80%,50%)` |  `hsl(80,80%,95%)` |  `hsl(80,80%,15%)` |
|   F   | `hsl(100,80%,50%)` | `hsl(100,80%,95%)` | `hsl(100,80%,15%)` |
|   G   | `hsl(120,80%,50%)` | `hsl(120,80%,95%)` | `hsl(120,80%,15%)` |
|   H   | `hsl(140,80%,50%)` | `hsl(140,80%,95%)` | `hsl(140,80%,15%)` |
|   I   | `hsl(160,80%,50%)` | `hsl(160,80%,95%)` | `hsl(160,80%,15%)` |
|   J   | `hsl(180,80%,50%)` | `hsl(800,80%,95%)` | `hsl(180,80%,15%)` |
|   K   | `hsl(200,80%,50%)` | `hsl(200,80%,95%)` | `hsl(200,80%,15%)` |
|   L   | `hsl(220,80%,50%)` | `hsl(220,80%,95%)` | `hsl(220,80%,15%)` |
|   M   | `hsl(240,80%,50%)` | `hsl(240,80%,95%)` | `hsl(240,80%,15%)` |
|   N   | `hsl(260,80%,50%)` | `hsl(260,80%,95%)` | `hsl(260,80%,15%)` |
|   O   | `hsl(280,80%,50%)` | `hsl(280,80%,95%)` | `hsl(280,80%,15%)` |
|   P   | `hsl(300,80%,50%)` | `hsl(300,80%,95%)` | `hsl(300,80%,15%)` |
|   Q   | `hsl(320,80%,50%)` | `hsl(320,80%,95%)` | `hsl(320,80%,15%)` |
|   R   | `hsl(340,80%,50%)` | `hsl(340,80%,95%)` | `hsl(340,80%,15%)` |
|   S   |    `hsl(0,0%,50%)` |    `hsl(0,0%,95%)` |    `hsl(0,0%,15%)` |

### 特記事項
- Android版ChromeやVivaldiでは,上部バーの色が変化する。
- アイコンは通常のアイコンとapple-touch-icon,Windowsタイルに対応。
- Internet Explorerでは利用できない。
- iOSのChromeでは適切に表示されない。
- iPhone X 対応。

### 更新内容
- 幾つか細かな部分のバグ修正,改良を実施

### 開く

| オンライン版 | オフライン版 |
|:----------:|:----------:|
| [A](https://akimikimikimikimikimikimika.github.io/Color/Color-A/ "Color A オンライン版") | [A](https://akimikimikimikimikimikimika.github.io/Color/Color-A/offline.html "Color A オフライン版") |
| [B](https://akimikimikimikimikimikimika.github.io/Color/Color-B/ "Color B オンライン版") | [B](https://akimikimikimikimikimikimika.github.io/Color/Color-B/offline.html "Color B オフライン版") |
| [C](https://akimikimikimikimikimikimika.github.io/Color/Color-C/ "Color C オンライン版") | [C](https://akimikimikimikimikimikimika.github.io/Color/Color-C/offline.html "Color C オフライン版") |
| [D](https://akimikimikimikimikimikimika.github.io/Color/Color-D/ "Color D オンライン版") | [D](https://akimikimikimikimikimikimika.github.io/Color/Color-D/offline.html "Color D オフライン版") |
| [E](https://akimikimikimikimikimikimika.github.io/Color/Color-E/ "Color E オンライン版") | [E](https://akimikimikimikimikimikimika.github.io/Color/Color-E/offline.html "Color E オフライン版") |
| [F](https://akimikimikimikimikimikimika.github.io/Color/Color-F/ "Color F オンライン版") | [F](https://akimikimikimikimikimikimika.github.io/Color/Color-F/offline.html "Color F オフライン版") |
| [G](https://akimikimikimikimikimikimika.github.io/Color/Color-G/ "Color G オンライン版") | [G](https://akimikimikimikimikimikimika.github.io/Color/Color-G/offline.html "Color G オフライン版") |
| [H](https://akimikimikimikimikimikimika.github.io/Color/Color-H/ "Color H オンライン版") | [H](https://akimikimikimikimikimikimika.github.io/Color/Color-H/offline.html "Color H オフライン版") |
| [I](https://akimikimikimikimikimikimika.github.io/Color/Color-I/ "Color I オンライン版") | [I](https://akimikimikimikimikimikimika.github.io/Color/Color-I/offline.html "Color I オフライン版") |
| [J](https://akimikimikimikimikimikimika.github.io/Color/Color-J/ "Color J オンライン版") | [J](https://akimikimikimikimikimikimika.github.io/Color/Color-J/offline.html "Color J オフライン版") |
| [K](https://akimikimikimikimikimikimika.github.io/Color/Color-K/ "Color K オンライン版") | [K](https://akimikimikimikimikimikimika.github.io/Color/Color-K/offline.html "Color K オフライン版") |
| [L](https://akimikimikimikimikimikimika.github.io/Color/Color-L/ "Color L オンライン版") | [L](https://akimikimikimikimikimikimika.github.io/Color/Color-L/offline.html "Color L オフライン版") |
| [M](https://akimikimikimikimikimikimika.github.io/Color/Color-M/ "Color M オンライン版") | [M](https://akimikimikimikimikimikimika.github.io/Color/Color-M/offline.html "Color M オフライン版") |
| [N](https://akimikimikimikimikimikimika.github.io/Color/Color-N/ "Color N オンライン版") | [N](https://akimikimikimikimikimikimika.github.io/Color/Color-N/offline.html "Color N オフライン版") |
| [O](https://akimikimikimikimikimikimika.github.io/Color/Color-O/ "Color O オンライン版") | [O](https://akimikimikimikimikimikimika.github.io/Color/Color-O/offline.html "Color O オフライン版") |
| [P](https://akimikimikimikimikimikimika.github.io/Color/Color-P/ "Color P オンライン版") | [P](https://akimikimikimikimikimikimika.github.io/Color/Color-P/offline.html "Color P オフライン版") |
| [Q](https://akimikimikimikimikimikimika.github.io/Color/Color-Q/ "Color Q オンライン版") | [Q](https://akimikimikimikimikimikimika.github.io/Color/Color-Q/offline.html "Color Q オフライン版") |
| [R](https://akimikimikimikimikimikimika.github.io/Color/Color-R/ "Color R オンライン版") | [R](https://akimikimikimikimikimikimika.github.io/Color/Color-R/offline.html "Color R オフライン版") |
| [S](https://akimikimikimikimikimikimika.github.io/Color/Color-S/ "Color S オンライン版") | [S](https://akimikimikimikimikimikimika.github.io/Color/Color-S/offline.html "Color S オフライン版") |
| [T](https://akimikimikimikimikimikimika.github.io/Color/Color-T/ "Color T オンライン版") | [T](https://akimikimikimikimikimikimika.github.io/Color/Color-T/offline.html "Color T オフライン版") |

### ソースコード
 [ソースコード (GitHub)](https://github.com/akimikimikimikimikimikimika/Color/ "ソースコード")

オンライン版では,全てのコンテンツを組み込み,常に最新の状態で利用できます。
オフライン版では,オンライン版と同じ体験をオフラインでもできるようにします。URLのdataスキームに全てのソースコードを埋め込んでいるので,一部コンテンツに制限があります。
